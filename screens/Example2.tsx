import React, { useState } from "react";
import { Pressable, StyleSheet } from "react-native";

import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";

interface Exercise {
  id: string;
  name: string;
}

const updateExerciseArray = (
  chosenExercises: string[],
  exerciseToChange: Exercise
) => {
  if (chosenExercises.includes(exerciseToChange.id)) {
    return chosenExercises.filter(
      (exerciseId) => exerciseId !== exerciseToChange.id
    );
  } else {
    return [...chosenExercises, exerciseToChange.id];
  }
};

export default function Example2() {
  const [exercises] = useState<Exercise[]>([
    { id: "1", name: "Pushups" },
    { id: "2", name: "Situps" },
    { id: "3", name: "Squats" },
    { id: "4", name: "Pullups" },
    { id: "5", name: "Leg Raises" },
    { id: "6", name: "Plank" },
    { id: "7", name: "Burpees" },
    { id: "8", name: "Jumping Jacks" },
    { id: "9", name: "Wall Sit" },
  ]);
  const [chosenExercises, setChosenExercises] = useState<string[]>(["1", "2"]);

  return (
    <View style={styles.container}>
      {exercises.map((exercise) => {
        return (
          <Pressable
            key={exercise.id}
            onPress={() => {
              setChosenExercises(
                updateExerciseArray(chosenExercises, exercise)
              );
            }}
            style={styles.row}
          >
            <Text style={styles.text}>{exercise.name}</Text>
            <Text style={styles.text}>
              {chosenExercises.includes(exercise.id) ? "Added" : "Not added"}
            </Text>
          </Pressable>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 50,
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  row: {
    paddingVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  text: {
    fontSize: 20,
  },
});
