import React, { useState } from "react";
import { Pressable, StyleSheet } from "react-native";

import { Text, View } from "../components/Themed";
import { RootTabScreenProps } from "../types";

interface Exercise {
  id: string;
  name: string;
  added: boolean;
}

const updateExerciseArray = (exercises: Exercise[], exercise: Exercise) => {
  const updatedExercises = [...exercises];

  return updatedExercises.map((updatedExercise) => {
    if (updatedExercise.id === exercise.id) {
      return { ...updatedExercise, added: !updatedExercise.added };
    }
    return updatedExercise;
  });
};

export default function Example1() {
  const [exercises, setExercises] = useState<Exercise[]>([
    { id: "1", name: "Pushups", added: false },
    { id: "2", name: "Situps", added: false },
    { id: "3", name: "Squats", added: false },
    { id: "4", name: "Pullups", added: false },
    { id: "5", name: "Leg Raises", added: false },
    { id: "6", name: "Plank", added: false },
    { id: "7", name: "Burpees", added: false },
    { id: "8", name: "Jumping Jacks", added: false },
    { id: "9", name: "Wall Sit", added: false },
  ]);

  return (
    <View style={styles.container}>
      {exercises.map((exercise) => {
        return (
          <Pressable
            key={exercise.id}
            onPress={() => {
              setExercises(updateExerciseArray(exercises, exercise));
            }}
            style={styles.row}
          >
            <Text style={styles.text}>{exercise.name}</Text>
            <Text style={styles.text}>
              {exercise.added ? "Added" : "Not added"}
            </Text>
          </Pressable>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 50,
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  row: {
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  text: {
    fontSize: 20,
  },
});
